import java.util.Arrays;
import java.util.Vector;



public class mainClass {

	public static void main(String[] args) {
		//int[] generatedArray = generateArray(10, 10);
		// System.out.println("Tablica: "+Arrays.toString(generatedArray));
		// System.out.println("Najmniejszy element:
		// "+getMinValueFromArray(generatedArray));
		// System.out.println("Suma cyfr: "+sumNumber(123));
		// System.out.println("Posortowana tablica:
		// "+Arrays.toString(bubbleSort(generatedArray)));
		// boolean[][] matrix=generateGnp(4, 1);
		// int[][] matrix=grafNK(5,5);
		// Klasa[] matrix=generateGraphAndTransformAS(5,(float)0.3);

		// System.out.println(graphTriangleCountForDamian(4,matrix));

		// for(int i=0;i<matrix.length;i++)
		// {System.out.println(matrix[i].tab.toString()+"\t"+ matrix[i].count);
		// }
		// for(int i=0;i<matrix.length;i++)
		// {System.out.println(Arrays.toString(matrix[i]));}

		// System.out.println(GrpahIntegrityInitiation(4));

		graphTransformation generateGraph = new graphTransformation(10,7);

		ColorGraph myColorGraph = letsColor(generateGraph);
		System.out.println("liczba kolorow: " + myColorGraph.count);
		System.out.println(Arrays.toString(myColorGraph.colors));
		System.out.println(Arrays.toString(myColorGraph.tabC));
		
		boolean[][] matrix=generateGraph.A;
		
		for(int index=0;index<matrix.length;index++){
			System.out.println(Arrays.toString(matrix[index]));
		}
		
		/*
		 * E2 [] e2Array = generateGnf(4, 2);
		 * 
		 * for(int i =0; i < e2Array.length; i++){ printE2Object(e2Array[i]);
		 * System.out.println(); } graphTransformation myGraphTransformation =
		 * new graphTransformation(2, 4);
		 */
	}

	public static ColorGraph letsColor(graphTransformation graf) {

		ColorGraph colors = new ColorGraph();

		int highestVal = graf.degSort[0];

		// graf.generateGraph();

		colors.tabC = new int[graf.vertex];
		while (highestVal > 0) {

			for (int i = 0; i < graf.deg.length; i++) {
				if (graf.deg[i] == highestVal) {
					colors.tabC[i] = checkColor(colors, graf.A, i);
				}
			}
			highestVal--;
		}
		colors.setX();
		return colors;

	}

	public static int checkColor(ColorGraph colors, boolean[][] tabAGraph, int vertexIndex) {
		int numberColor = 1;
		boolean setColor = false;

		do {
			setColor=false;
			for (int row = 0; row < tabAGraph.length; row++) {
				if (tabAGraph[row][vertexIndex] == true && colors.tabC[row] == numberColor) {
					numberColor++;
					setColor = true;
					break;
				}
			}
		} while (setColor);

		return numberColor;
	}

	public static void printE2Object(E2 object) {
		System.out.print("A: " + object.a + " B: " + object.b + " C: " + object.c);
	}

	public static E2[] generateGnf(int N, int F) {

		int h = N * (N - 1) / 2;
		int[][] E = grafNK(N, h);
		int[] C = new int[N];

		E2[] resultE = new E2[h];
		for (int i = 0; i < h; i++) {
			resultE[i] = new E2(E[i][0], E[i][1], false);
		}

		do {

			int z_indeks = (int) Math.floor(Math.random() * h);

			if (C[resultE[z_indeks].a - 1] < F && C[resultE[z_indeks].b - 1] < F) {

				C[resultE[z_indeks].a - 1]++;
				C[resultE[z_indeks].b - 1]++;

				resultE[z_indeks].c = true;
				E2 tempObject = resultE[z_indeks];
				resultE[z_indeks] = resultE[h - 1];
				resultE[h - 1] = tempObject;
			}

			h--;
		} while (h > 0);
		return resultE;
	}

	public static boolean GraphIntegrityInitiation(int N) {
		boolean[] verticalesArray = new boolean[N];
		return GraphIntegrityCheck(N, 1, generateGnp(N, (float) 0.9), verticalesArray);

	}

	public static boolean GraphIntegrityCheck(int N, int v, boolean[][] A, boolean[] X) {
		System.out.println(v);
		if (v == (N + 1)) {
			for (int j = 0; j < X.length; j++) {
				if (X[j] == false) {

					System.out.println(Arrays.toString(X));
					return false;
				}
			}
			System.out.println(Arrays.toString(X));
			return true;
		}
		for (int i = 1; i < N; i++) {
			if (A[v][i] && X[i] == false) {
				X[i] = true;
				return GraphIntegrityCheck(N, i, A, X);
			}

		}
		return false;
	}

	public static int graphTriangleCountForDamian(int N, boolean[][] A) {

		int triangle = 0;
		Klasa[] helpArray = transformAS(N, A);

		// for(int i=0;i<helpArray.length;i++)
		// {System.out.println(helpArray[i].tab.toString()+"\t"+
		// helpArray[i].count);
		// }

		Vector<Integer[]> trianglesList = new Vector<>();
		Integer[] tempArray = new Integer[3];
		for (int i = 0; i < helpArray.length - 1; i++) {
			for (int j = i + 1; j < helpArray[i].tab.size(); j++) {
				Vector<Integer> array1 = helpArray[i].tab;
				Vector<Integer> array2 = helpArray[j].tab;

				for (int k = 0; k < array1.size(); k++) {
					if (array1.get(k) != null && array2.indexOf(array1.get(k)) >= 0 && array1.get(k) > j) {

						tempArray[0] = i + 1;
						tempArray[1] = j + 1;
						tempArray[2] = array1.get(k);
						System.out.println(Arrays.toString(tempArray));
						trianglesList.add(tempArray);
						triangle++;
					}
				}
			}

		}
		// for(int i=0;i<trianglesList.size();i++)
		// {
		// System.out.println(Arrays.toString(trianglesList.get(i)));
		// }

		return triangle;

	}

	public static Klasa[] generateGraphAndTransformAS(int N, float P) {
		boolean pomTab[][] = generateGnp(N, P);
		return transformAS(N, pomTab);
	}

	public static Klasa[] transformAS(int N, boolean[][] A) {
		Klasa[] s = new Klasa[N];
		for (int i = 0; i < N; i++) {
			s[i] = new Klasa();
		}

		for (int i = 0; i < N - 1; i++) {
			for (int j = i + 1; j < N; j++) {
				if (A[i][j]) {
					s[i].count++;
					s[i].tab.add(j + 1);
					s[j].tab.add(i + 1);
					s[j].count++;
				}
			}
		}

		return s;
	}

	public static int[][] generateGraphAndTransformAE(int N, float P) {
		boolean pomTab[][] = generateGnp(N, P);
		return transformAE(pomTab);
	}

	public static int[][] transformAE(boolean[][] A) {
		int N = A.length;
		int h = N * (N - 1) / 2;
		int[][] E = new int[h][3];

		int indexE = 0;
		for (int i = 0; i < N - 1; i++) {
			for (int j = i + 1; j < N; j++) {
				E[indexE][0] = i + 1;
				E[indexE][1] = j + 1;
				E[indexE][2] = (A[i][j]) ? 1 : 0;
				indexE++;
			}
		}

		return E;
	}

	public static boolean[][] generateGraphAndTransform(int N, int K) {
		int pomTab[][] = grafNK(N, K);
		return transformEA(pomTab, K, N);
	}

	public static boolean[][] transformEA(int[][] E, int h, int size) {
		boolean[][] A = new boolean[size][size];
		for (int i = E.length - 1; i >= E.length - h; i--) {
			A[E[i][0] - 1][E[i][1] - 1] = true;
			A[E[i][1] - 1][E[i][0] - 1] = true;
		}
		return A;
	}

	public static int[][] grafNK(int N, int K) {
		int h = N * (N - 1) / 2;
		int[][] matrix = new int[h][2];
		int indexMatrix = 0;
		// zapisanie wszytkich kombinacji krawedzi
		for (int i = 0; i < N - 1; i++) {
			for (int j = i + 1; j < N; j++) {
				matrix[indexMatrix][0] = i + 1;
				matrix[indexMatrix][1] = j + 1;
				indexMatrix++;
			}
		}
		// losowanie krawedzi i przepisanie wyniku na koniec
		for (int iloscKrawedzi = 0; iloscKrawedzi < K; iloscKrawedzi++) {
			int losowanieKrawedzi = (int) Math.floor(Math.random() * h);
			// zamianna
			int temp[] = matrix[h - 1];
			matrix[h - 1] = matrix[losowanieKrawedzi];
			matrix[losowanieKrawedzi] = temp;

			h--;
		}
		// przepisanie wynikow
		// int wynik[][] =new int[matrix.length-h][2];
		// for (int
		// indexWnik=0,indexMatrix2=h;indexMatrix2<matrix.length;indexMatrix2++,indexWnik++){
		// wynik[indexWnik]=matrix[indexMatrix2];
		// }

		return matrix;
	}

	public static boolean[][] generateGnp(int size, float probability) {
		boolean[][] result = new boolean[size][size];

		for (int i = 0; i < result.length - 1; i++) {
			for (int j = i + 1; j < result.length; j++) {
				if (Math.random() < probability) {
					result[i][j] = true;
					result[j][i] = true;
				}
			}
		}
		return result;
	}

	public static int[] bubbleSort(int[] items) {
		boolean anySwap = false;
		do {
			anySwap = false;
			for (int i = 0; i < items.length - 1; i++) {
				if (items[i] > items[i + 1]) {
					int tempValue = items[i + 1];
					items[i + 1] = items[i];
					items[i] = tempValue;
					anySwap = true;
				}
			}
		} while (anySwap);

		return items;
	}

	public static int sumNumber(int numberToSum) {
		int sum = 0;
		while (numberToSum > 0) {
			sum = sum + (numberToSum % 10);
			numberToSum = numberToSum / 10;
		}
		return sum;
	}

	public static int[] generateArray(int size, int range) {
		int[] returnedArray = new int[size];
		for (int i = 0; i < size; i++) {
			returnedArray[i] = (int) Math.floor(Math.random() * range + 1);
		}
		return returnedArray;
	}

	public static int getMinValueFromArray(int[] items) {
		int minValue = items[0];
		for (int i = 1; i < items.length; i++) {
			if (items[i] < minValue) {
				minValue = items[i];
			}
		}
		return minValue;
	}
}