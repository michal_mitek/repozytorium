import java.util.Arrays;


public class graphTransformation {

	public graphTransformation(){
		
	}
	
	public graphTransformation(int edge, int vertex) {
		super();
		this.edge = edge;
		this.vertex = vertex;
		this.generateE2();
		this.generateGraph();
	}

	int edge = 0;
	int vertex = 0;
	E2[] grapE;
	boolean[][] A;
	int[] deg;
	int[] degSort;

	public void generateE2() {

		deg = new int[vertex];
		degSort = new int[vertex];
		int h = vertex * (vertex - 1) / 2;
		int[][] E = mainClass.grafNK(vertex, h);
		this.A = new boolean[vertex][vertex];
		grapE = new E2[h];
		for (int i = 0; i < h; i++) {
			grapE[i] = new E2(E[i][0], E[i][1], false);
		}

		for (int i = 0; i < vertex - 1; i++) {
			for (int j = i + 1; j < vertex; j++) {
				if (A[i][j] == true) {

					deg[i]++;
					deg[j]++;
				}
			}
		}
		degSort = deg;
		System.out.println(Arrays.toString(deg));
		System.out.println(Arrays.toString(degSort));
	}

	public void generateGraph() {

		int h = vertex * (vertex - 1) / 2;
		for (int iloscKrawedzi = 0; iloscKrawedzi < this.edge; iloscKrawedzi++) {
			int losowanieKrawedzi = (int) Math.floor(Math.random() * h);
			E2 temp = this.grapE[losowanieKrawedzi];
			temp.c = true;
			deg[temp.a - 1]++;
			deg[temp.b - 1]++;
			A[temp.a - 1][temp.b - 1] = true;
			A[temp.b - 1][temp.a - 1] = true;
			this.grapE[losowanieKrawedzi] = this.grapE[h - 1];
			this.grapE[h - 1] = temp;
			h--;
		}
		degSort = deg.clone();
		degSort = arrayDegSort(degSort);
		System.out.println(Arrays.toString(deg));
		System.out.println(Arrays.toString(degSort));
	}

	public int[] arrayDegSort(int[] degSort) {

		boolean flag = false;
		for (int i = 0; i < degSort.length-1; i++) {
			if (degSort[i] < degSort[i + 1]) {
				int tempIndex = degSort[i];
				degSort[i] = degSort[i + 1];
				degSort[i + 1] = tempIndex;
				flag = true;
			}

		}
		if (flag) {
			return arrayDegSort(degSort);
		}

		return degSort;
	}
}
