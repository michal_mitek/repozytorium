function Board(n, m) {
    var dim1 = n;
    var dim2 = m;
    var table = [];
    var oldSnake = null;
    
    for (var i = 0; i < dim1; i++) {
        table[i] = [];
        for (var j = 0; j < dim2; j++) {
            table[i][j] = 0;
        }
    }
    
    this.drawBoard = function() {
        $('.uberDiv').remove();
        var uberDiv = $("<div class='uberDiv'/>");
        for (var i = 0; i < dim1; i++) {
            var rowDiv = $("<div class='rowDiv'/>");
            for (var j = 0; j < dim2; j++) {
                var div = $("<div class='boardCell'/>");
                if (table[i][j] === 1) {
                    div.addClass('isSnake');
                }
                if (table[i][j] === 2) {
                    div.addClass('isFood');
                }
                rowDiv.append(div);
            }
            uberDiv.append(rowDiv);
        }
        $('body').append(uberDiv);
    }
    
    this.drawSnake = function(params) {
        var param = params[0];
        
        var result = -1;
        
        if (param[0] < 0 || param[1] < 0 ||
            dim1 <= param[0] || dim2 <= param[1]) {
            result = 0;
        } else {
            
            if (table[param[0]][param[1]] === 2) {
                this.generateFood();
                result = 2;
                
            }
            
            clearSnake();
            oldSnake = [];
            
            for(var i = 0; i<params.length; i++){
                oldSnake.push(params[i]);
            }
            
            for(var i = 0; i<params.length;i++){
                table[params[i][0]][params[i][1]] = 1;
            }
            this.drawBoard();
            if(result === -1){
                result = 1;
            }
            
        }
        
        return result;
    }
    
    this.generateFood = function() {
        var found = false;
        while (!found) {
            var x = Math.floor(Math.random() * dim1);
            var y = Math.floor(Math.random() * dim2);

            if (table[x][y] === 0) {
                table[x][y] = 2;
                found = true;
            } 
        }
        this.drawBoard();
    }
    
    function clearSnake() {
        if (oldSnake !== null) {
            for(var i=0; i< oldSnake.length; i++){
                table[oldSnake[i][0]][oldSnake[i][1]] = 0;
            }
        
        }
    }
}

function Snake() {
    var position = [];
    var direction = 'down';
    var grow = false;
    
    this.placeSnake = function(x1,y1) {
        position.push([x1, y1]);
    }
    
    this.getSnake = function() {
        return position;
    }
    
    
    this.growSnake = function(){
        grow = true;
    }
    this.move = function() {
        var POM = [position[0][0], position[0][1]];
        switch (direction) {
            case 'down': POM[0]++; break;
            case 'up': POM[0]--; break;
            case 'left': POM[1]--; break;
            case 'right': POM[1]++; break;
        }
        var POM_TAB = [];
        POM_TAB.push(POM);
        
        var pomLength;
        
        if(grow) {
           
            pomLength = position.length;
            
           } else {
               
            pomLength = position.length - 1;
           
           }
        for(i=0; i<pomLength; i++){
            POM_TAB.push(position[i])
        }
        position = POM_TAB;
        grow = false;
    }
    
    //down, up, left, right
    this.changeDirection = function(newDirection) {
        direction = newDirection;
    }
};

$(document).ready(function() {
    var gameBoard = new Board(10,10);
    var snake = new Snake();   
   
    $('.play').click(function() {
        snake.placeSnake(0,0);
        snake.growSnake();
        gameBoard.drawSnake(snake.getSnake());
        gameBoard.generateFood();
        
        setInterval(function(){
            snake.move();
            var pom = gameBoard.drawSnake(snake.getSnake());
            if (pom === 0) {
                alert('Przegrałeś!');
            } else if (pom === 2){
                snake.growSnake();
            }
               

            },250 )
    });
    
    $('body').keyup(function(e){
        switch (e.keyCode){
            case 37: snake.changeDirection('left'); break;
            case 38: snake.changeDirection('up'); break;
            case 39: snake.changeDirection('right'); break;
            case 40: snake.changeDirection('down'); break;
        }    
    
    });
    
    $('.move').click(function() {
        snake.move();
        var pom = gameBoard.drawSnake(snake.getSnake());
        if (pom === 0) {
            alert('Przegrałeś!');
        } else if (pom === 2){
            snake.growSnake();
        }
    });
    
    $('.up').click(function() {
        snake.changeDirection('up');
    });
    
    $('.down').click(function() {
        snake.changeDirection('down');
    });
    
    $('.left').click(function() {
        snake.changeDirection('left');
    });
    
    $('.right').click(function() {
        snake.changeDirection('right');
    });
})