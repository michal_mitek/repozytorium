package service;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import controller.UsersController;
import model.UsersModel;
import util.AppController;

@WebServlet("/UsersServlet")
public class UsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public UsersServlet() { }

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		if (action.equalsIgnoreCase("logout")) {
			session.removeAttribute("login");
			session.removeAttribute("password");
			response.sendRedirect("index.jsp");
		} else if (action.equalsIgnoreCase("edit")) {
			session.setAttribute("userId", request.getParameter("id"));
			response.sendRedirect("editUser.jsp");
		} else if (action.equalsIgnoreCase("delete")) {
			UsersController zc = new UsersController(0);
			zc.deleteUser(request.getParameter("id"));
			response.sendRedirect("users.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		if (action.equalsIgnoreCase("login")) {
			String login = request.getParameter("login");
			String password = request.getParameter("password");
			UsersController zc = new UsersController(login, password);
			boolean isLogged = zc.checkLogin();

			if (isLogged) {
				session.setAttribute("login", login);
				session.setAttribute("password", password);
				session.setAttribute("access", zc.getRola());
				response.sendRedirect("welcome.jsp");
			} else {
				response.sendRedirect("error.jsp");
			}
		} else if (action.equalsIgnoreCase("add")) {
			String login = request.getParameter("login");
			String password = request.getParameter("password");
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			int role = Integer.parseInt( request.getParameter("access") );
			UsersModel newUser = new UsersModel(login, password, name, surname, role);
			UsersController zc = new UsersController(0);
			zc.addUser(newUser);
			response.sendRedirect("users.jsp");
		} else if (action.equalsIgnoreCase("edit")) {
			String userId = request.getParameter("id");
			UsersController zc = new UsersController(0);
			zc.updatePassword(request.getParameter("password"), userId);
			session.removeAttribute("userId");
			response.sendRedirect("users.jsp");
		}
	}

}
