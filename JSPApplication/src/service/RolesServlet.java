package service;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controller.RolesController;

@WebServlet("/RolesServlet")
public class RolesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RolesServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		if (action.equalsIgnoreCase("delete")) {
			int roleId = Integer.parseInt(request.getParameter("id"));
			RolesController rc = new RolesController(roleId);
			rc.deleteRole();
			response.sendRedirect("roles.jsp");
		}else if(action.equalsIgnoreCase("edit")){
			HttpSession session = request.getSession();
			session.setAttribute("editId", request.getParameter("id"));
			response.sendRedirect("editRole.jsp");
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action.equalsIgnoreCase("edit")){
			HttpSession session = request.getSession();
			
			int roleId = Integer.parseInt(request.getParameter("roleId"));
			String roleName = request.getParameter("roleName");
			RolesController rc = new RolesController(roleId, roleName);
			rc.editRole();
			session.removeAttribute("editId");
			response.sendRedirect("roles.jsp");
			
		}
	}

}
