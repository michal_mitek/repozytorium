package service;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controller.TasksController;

@WebServlet("/TasksServlet")
public class TasksServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TasksServlet() { }

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		if (action.equalsIgnoreCase("delete")) {
			TasksController zc = new TasksController();
			zc.deleteTask(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect("tasks.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		if (action.equalsIgnoreCase("add")) {
			String tytul = request.getParameter("title");
			String tresc = request.getParameter("content");

			TasksController zc = new TasksController();

			zc.insertTask(0, session.getAttribute("login").toString(), tytul, tresc);

			response.sendRedirect("tasks.jsp");
		}
	}

}
