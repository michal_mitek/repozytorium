<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE hibernate-configuration PUBLIC
        "-//Hibernate/Hibernate Configuration DTD 3.0//EN"
        "http://hibernate.sourceforge.net/hibernate-configuration-3.0.dtd">
 
<hibernate-configuration>
    <session-factory>
        <!-- Database connection settings -->
        <property name="dialect">org.hibernate.dialect.PostgreSQLDialect</property>
        <property name="connection.driver_class">org.postgresql.Driver</property>
        <property name="connection.username">1035_tomasz_lysi</property>
        <property name="connection.password">dpn2dnvC</property>
        <property name="connection.url">jdbc:postgresql://pdb102.ansta.pl:52345/1035_tomasz_lysi</property>
        
        <!-- JDBC connection pool (use the built-in) -->
        <property name="connection.pool_size">20</property>
        <property name="current_session_context_class">thread</property>

        <!-- Disable the second-level cache  -->
        <property name="cache.provider_class">org.hibernate.cache.NoCacheProvider</property>
        
        <!-- Echo all executed SQL to stdout -->
        <property name="show_sql">true</property>

        <!-- Drop the existing tables and create new one -->
        <property name="hbm2ddl.auto">create</property>
 
        <!-- Mention here all the model classes along with their package name -->
 		<mapping class="model.UsersModel"/>
 		<mapping class="model.TasksModel"/>
 		<mapping class="model.RolesModel"/>
 		 
    </session-factory>
</hibernate-configuration>
