package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tasks")
public class TasksModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String login;
	private String content;
	private String title;


	public TasksModel() {

	}

	public TasksModel(int id, String login, String tytul, String tresc) {
		this.id = id;
		this.login = login;
		this.content = tresc;
		this.title = tytul;
	}

	public void setTitle(String tytul) {
		this.title = tytul;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String tresc) {
		this.content = tresc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

}
