package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class UsersModel {

	@Id
	private String login;
	private String password;
	private String name;
	private String surname;
	private int access;

	public UsersModel() {
	}

	public UsersModel(String login, String haslo, String imie, String nazwisko, int uprawnienia) {
		super();
		this.login = login;
		this.password = haslo;
		this.name = imie;
		this.surname = nazwisko;
		this.access = uprawnienia;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String haslo) {
		this.password = haslo;
	}

	public int getAccess() {
		return access;
	}

	public void setAccess(int uprawnienia) {
		this.access = uprawnienia;
	}

	public String getName() {
		return name;
	}

	public void setName(String imie) {
		this.name = imie;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String nazwisko) {
		this.surname = nazwisko;
	}

}
