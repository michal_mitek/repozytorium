package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="userRoles")
public class RolesModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String access;
	
	public RolesModel() { }

	public RolesModel(int id, String role) {
		super();
		this.id = id;
		this.access = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String role) {
		this.access = role;
	}

	@Override
	public String toString() {
		return "RolesModel [id=" + id + ", role=" + access + "]";
	}
	
	
}
