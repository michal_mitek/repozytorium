package controller;

import model.*;
import util.AppController;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import java.util.Iterator;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class UsersController {

	private String login;
	private String haslo;
	private int rola; 

	public UsersController() {
		this.initializeSF();
	}

	public UsersController(int f) {
		if (f == 1) {
			this.initializeSF();
		}
	}

	public UsersController(String login, String haslo) {
		this.login = login;
		this.haslo = haslo;
		this.initializeSF();
	}

	public int getRola() {
		return rola;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public boolean checkLogin() {

		boolean f = false;
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();

			List data = s.createQuery("FROM UsersModel ORDER BY login DESC").list();
			for (Iterator i = data.iterator(); i.hasNext();) {
				UsersModel u = (UsersModel) i.next();
				if (u.getLogin().equalsIgnoreCase(this.login) && u.getPassword().equalsIgnoreCase(this.haslo)) {
					this.rola=u.getAccess();
					f = true;
					break;
				}
			}
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return f;

	}

	public void updatePassword(String password, String login) {
		try {
			Session s = AppController.getSession().openSession();
			Transaction comit = s.beginTransaction();
			UsersModel user = s.get(UsersModel.class, login);
			user.setPassword(password);
			s.update(user);
			comit.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void deleteUser(String kto) {

		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();

			UsersModel user = s.get(UsersModel.class, kto);
			s.delete(user);
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void initializeSF() {
		
	}

	public List<UsersModel> returnUsers() {
		List data = null;
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();

			data = s.createQuery("FROM UsersModel").list();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return data;
	}
	
	public void addUser(UsersModel user) {
		try {
			Session s = AppController.getSession().openSession();
			Transaction comit = s.beginTransaction();
			s.save(user);
			comit.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}


}
