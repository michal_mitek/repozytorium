package controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import model.RolesModel;
import util.AppController;

public class RolesController {
	private RolesModel rolesModel;

	public RolesController() {

	}

	public RolesController(int id, String name) {

		this.rolesModel = new RolesModel(id, name);
	}
	
	public RolesController(int id) {

		this.rolesModel = new RolesModel(id, "");
	}


	public List<RolesModel> getRoles() {
		Session s = AppController.getSession().openSession();

		List<RolesModel> rolesList = null;
		try {
			rolesList = s.createQuery("FROM RolesModel").list();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
		return rolesList;
	}

	public void addRole() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();
		try {
			s.save(this.rolesModel);
			t.commit();

		} catch (Exception e) {
			if (t != null) {
				t.rollback();
			}
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}

	public void deleteRole() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();

		try {
			s.delete(s.get(RolesModel.class, this.rolesModel.getId()));
			t.commit();
		} catch (Exception e) {
			if (t != null) {
				t.rollback();
			}
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}

	public void editRole() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();

		try {
			s.update(this.rolesModel);
			t.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}

}
