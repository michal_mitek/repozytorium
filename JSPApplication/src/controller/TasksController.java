package controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.TasksModel;
import model.UsersModel;
import util.AppController;

public class TasksController {

	private int id;
	private String login;
	private String tresc;
	private String tytul;

	public TasksController() {
		super();
	}

	public TasksController(int id, String login, String tresc, String tytul) {
		this.id = id;
		this.login = login;
		this.tresc = tresc;
		this.tytul = tytul;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getTresc() {
		return tresc;
	}

	public void setTresc(String tresc) {
		this.tresc = tresc;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}
	
	public void insertTask(int id, String login, String title, String text){
		try{
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			
			TasksModel tm = new TasksModel(id, login, title, text);
			
			s.save(tm);
			t.commit();
			s.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<TasksModel> getRecords(String login) {
		List data = null;
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();

			data = s.createQuery("FROM TasksModel WHERE login = :login").setString("login", login).list();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return data;
	}
	
	public void deleteTask(int id) {

		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();

			TasksModel taskModel = s.get(TasksModel.class, id);
			s.delete(taskModel);
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
