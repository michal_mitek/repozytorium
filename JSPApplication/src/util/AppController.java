package util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AppController {

	public static SessionFactory sf;
	public static boolean newDB = true;
	
	public static SessionFactory getSession() {
		return AppController.sf;
	}
	
	public static void setSession(){
		if(AppController.newDB == true) {
			try {
				AppController.sf = new Configuration().configure().buildSessionFactory();
				AppController.newDB = false;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}
}