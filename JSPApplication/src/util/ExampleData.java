package util;

import org.hibernate.Session;
import org.hibernate.Transaction;
import model.*;

public class ExampleData {

	public ExampleData() {
		this.initUsers();
		this.initTasks();
		this.initRoles();
	}
	
	public void initUsers() {
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			UsersModel um = new UsersModel("a", "bb", "Tomek", "Mistrzowski", 1);
			UsersModel um2 = new UsersModel("aa", "cc", "Pawel", "Mazak", 2);
			s.save(um);
			s.save(um2);
			t.commit();
			s.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void initTasks() {
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			TasksModel tm1 = new TasksModel(0, "a", "Zad1", "test1");
			TasksModel tm2 = new TasksModel(0, "a", "Zad2", "test2");
			TasksModel tm3 = new TasksModel(0, "aa", "Zad3", "test11");
			TasksModel tm4 = new TasksModel(0, "aa", "Zad4", "test22");
			
			s.save(tm1);
			s.save(tm2);
			s.save(tm3);
			s.save(tm4);
			
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void initRoles(){
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			
			RolesModel rm1 = new RolesModel(1, "Admin");
			RolesModel rm2 = new RolesModel(2, "User");
			s.save(rm1);
			s.save(rm2);
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
