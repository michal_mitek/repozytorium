<%@ page 
	language="java" 
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.List, model.*, java.util.Iterator, controller.*"
%>
<%
	boolean isAdmin = session.getAttribute("access").toString().equals("1");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<h2>Zadania</h2>
	<a href="./UsersServlet?action=logout">Wyloguj</a>
	<div>
		<form method="POST" action="./TasksServlet?action=add">
			Tytul: <input type="text" name="title" /><br /> Tresc:
			<textarea type="text" name="content"></textarea>
			<br /> <input type="submit" value="Dodaj" />
		</form>
	</div>

	<%
		TasksController zco = new TasksController();

		List<TasksModel> taskList = zco.getRecords((String) session.getAttribute("login"));
		int z = 1;
		for (Iterator j = taskList.iterator(); j.hasNext();) {
			TasksModel tm = (TasksModel) j.next();
	%>
	<table width="100%" cellpadding="2" border="1">
		<tr>
			<td>
				<div>
					<%
						out.println(tm.getId());
					%>
					<%
						out.println(tm.getTitle());
					%>

					<%
						out.println(tm.getContent());
					%>
					<a href="./TasksServlet?action=delete&id=<%out.println(tm.getId());%>">Usun</a>
				</div>
			<td>
		</tr>
	</table>
	<%
		z++;
		}
	%>
</body>
</html>