<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.List, model.*, java.util.Iterator, controller.*"
%>
<%
	boolean isAdmin = session.getAttribute("access").toString().equals("1");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h2>Zarzadzanie Rolami</h2>

	<h3>Lista rol</h3>
	<table>
		<tr>
			<th>#</th>
			<th>Nazwa</th>
			<th>Ilosc uzytkownikow</th>
			<th>Akcja</th>
		</tr>
		<%
			List<RolesModel> roles = new RolesController().getRoles();
			for (Iterator i = roles.iterator(); i.hasNext();) {
			RolesModel currentRole = (RolesModel)i.next();
		%>
		<tr>
			<td><% out.print(currentRole.getId()); %></td>
			<td><% out.print(currentRole.getAccess()); %></td>
			<td>3</td>
			<td>
			<a href="./RolesServlet?action=edit&id=<%out.println(currentRole.getId());%>">Edycja</a> / 
			<a href="./RolesServlet?action=delete&id=<%out.println(currentRole.getId());%>">Usun</a>
			</td>
		</tr>
		<%
			}
		%>
	</table>

	<h3>Dodaj Role</h3>
	<div>
		<form method="POST">
			Nazwa:<br /> <input type="text" name="nameRole" /> <input
				type="submit" value="Dodaj" />

		</form>
	</div>



</body>
</html>