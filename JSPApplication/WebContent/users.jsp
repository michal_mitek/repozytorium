<%@ page 
	language="java" 
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.List, model.*, java.util.Iterator, controller.*"
%>
<%
	boolean isAdmin = session.getAttribute("access").toString().equals("1");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h2>Uzytkownicy</h2>
	<a href="./UsersServlet?action=logout">Wyloguj</a>
	<table width="100%" cellpadding="2" border="1">
		<tr>
			<th>Id</th>
			<th>Login</th>
			<% if (isAdmin) { %>
			<th>Rola</th>
			<th>Hasło</th>
			<% } %>
			<th>Imie</th>
			<th>Nazwisko</th>
			<% if (isAdmin) { %>
			<th>Akcja</th>
			<% } %>
		</tr>
		<%
			UsersController zc = new UsersController(0);
			List<UsersModel> usersList = zc.returnUsers();
			int k = 0;
			for (Iterator i = usersList.iterator(); i.hasNext();) {
				UsersModel user = (UsersModel) i.next();
				k++;
		%>
		<tr>
			<td>
				<% out.println(k); %>
			</td>
			<td>
				<% out.println(user.getLogin()); %>
			</td>
			<% if (isAdmin) { %>
			<td>
				<%
					int rola = user.getAccess();
					if (rola == 1) {
						out.println("Admin");
					} else if (rola == 2) {
						out.println("User");
					} else {
						out.println("sprzataczka");
					}
				%>
			</td>
			<td>
				<% out.println(user.getPassword()); %>
			</td>
			<% } %>
			<td>
				<% out.println(user.getName()); %>
			</td>
			<td>
				<% out.println(user.getSurname()); %>
			</td>
			<% if (isAdmin) { %>
			<td><a
				href="./UsersServlet?action=edit&id=<%out.println(user.getLogin());%>">Edycja</a>
				&bull; <a
				href="./UsersServlet?action=delete&id=<%out.println(user.getLogin());%>">Usun</a>

			</td>
			<% } %>
		</tr>
		<% } %>
	</table>
	<% if (isAdmin) { %>
	<h2>Dodaj uzytkownika</h2>
	<div>
		<form method="POST" action="./UsersServlet?action=add">
			Login: <input type="text" name="login" /> Haslo: <input
				type="password" name="password" /> Imie: <input type="text"
				name="name" /> Nazwisko: <input type="text" name="surname" /> <select
				name="access">
				<option value="1">Admin</option>
				<option value="2">User</option>
			</select> <input type="submit" value="Dodaj" />
		</form>
	</div>
	<% } %>
</body>
</html>