import java.util.Scanner;

public class Game {
	private Grid grid;
	private Player player1;
	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	private Player player2;
	
	public Game() {
		grid = new Grid();
		grid.setWidth(10);
		grid.setHeight(10);
		grid.initGameBoard();
	}
	
	public void addPlayers() {
		player1 = new Player();
		player2 = new Player();
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Podaj imi� gracza numer 1:");
		player1.setName(scanner.nextLine());
		System.out.println("Podaj znak");
		player1.setSign(scanner.next().charAt(0));
		scanner.nextLine();
		
		System.out.println("Podaj imi� gracza numer 2:");
		player2.setName(scanner.nextLine());
		System.out.println("Podaj znak");
		player2.setSign(scanner.next().charAt(0));
		scanner.nextLine();
		
		System.out.println(player1.toString());
		System.out.println(player2.toString());		
		
	}
	
	public void displayGameFromGrid() {
		grid.displayBoard(player1.getSign(), player2.getSign());
	}
	
	public void play () {
		boolean switchPlayer = false;
		Scanner scanner = new Scanner(System.in);
		int X = 0;
		int Y = 0;
		do {
			System.out.println("Podaj X");
			 X = Integer.parseInt(scanner.nextLine());
			
			System.out.println("Podaj Y");
			 Y = Integer.parseInt(scanner.nextLine());
			
			if(switchPlayer == true){
				grid.updateBoard(X, Y, 1);
				switchPlayer = false;
			}
			else{
				grid.updateBoard(X, Y, 2);
				switchPlayer = true;
			}
			displayGameFromGrid();
			
		
		} while (!isAnyWinner(X, Y, switchPlayer) || !fullBoard());
		System.out.println(checkWhoIsTheWinner(switchPlayer).toString());
	}
	
	public boolean fullBoard (){
		
		int [][] table = grid.getGameBoard();
		for (int i=0; i<table.length;i++){
			for(int j=0; j<table.length; j++){
				if(table[i][j]==0){
					return false;
				}
			}
		}
		return true;
	}
	
	public Player checkWhoIsTheWinner(boolean switchFlag){
		if(switchFlag == true){
			return player1;
		}
		else{
			return player2;
		}
	}
		
	public boolean isAnyWinner(int x, int y, boolean switchFlag) {
		int player = 0;
		
		if(switchFlag == true){
			player = 1;
		}else{
			player = 2;
		}
		
		int [][] tempGameBoard = grid.getGameBoard();
		boolean  flag = false;
		int counter = 0;
		for(int i = x-2; i <= x+2; i++){
			if(i >= 0 && i>tempGameBoard.length &&  tempGameBoard[i][y] == player){
				counter++;
				if(counter ==3){
					return true;
				}
			}else{
				counter = 0;
			}
			counter = 0;
		}
		
		for(int i = x-2; i <= x+2; i++){
			if(i >= 0 && i>tempGameBoard.length &&  tempGameBoard[i][y] == player){
				counter++;
				if(counter ==3){
					return true;
				}
			}else{
				counter = 0;
			}
			counter = 0;
		}
		
		return flag;
	}
}

