
public class Player {

	private String name;
	private char sign;

	@Override
	public String toString() {
		return "Player [name=" + name + ", sign=" + sign + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getSign() {
		return sign;
	}

	public void setSign(char sign) {
		this.sign = sign;
	}
}
