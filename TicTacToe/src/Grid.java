
public class Grid {
	
	private int width;
	private int height;
	private int[][] gameBoard;
	
	public int[][] getGameBoard() {
		return gameBoard;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}


	public int getWidth() {
		return width;
	}

	public void setWidth(int width){
		this.width = width;
	}
	
	public void initGameBoard(){
		gameBoard = new int[width][height];
	}
	
	public void displayBoard(char signPlayerOne, char signPlayerTwo) {
		for (int column = 0; column < width; column++){
			System.out.print(column +"|");	
		}
		System.out.println();
		
		for (int i = 0; i < width; i++ ){
			for (int j = 0; j < height; j ++){
				switch (gameBoard[i][j]) {
				case 0: System.out.print(" |");
					
					break;
				case 1: System.out.print(signPlayerOne + "|" );
					break;
				case 2: System.out.print(signPlayerTwo + "|" );
					break;
				}
			}
			System.out.println(i);
		}
		System.out.println();
	}
	
	public void updateBoard (int X, int Y, int numberPlayer) {
		if((X > 0 && X < width)&&(Y > 0 && Y < height) && gameBoard[X][Y] == 0){
			 gameBoard[X][Y] = numberPlayer;
			}
		}
}
