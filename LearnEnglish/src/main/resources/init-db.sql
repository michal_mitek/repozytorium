INSERT INTO user(id, login, password, full_name, role) values (nextval('user_seq'), 'login', 'login', 'John Example', 'ROLE_USER');
INSERT INTO user(id, login, password, full_name, role) values (nextval('user_seq'), 'admin', 'admin', 'John Admin', 'ROLE_ADMIN');

INSERT INTO flash_cards_categories(id, category, type) values(nextval('flash_cards_cat_seq'), 'Animals', 'TYPE_GLOBAL');
INSERT INTO flash_cards_categories(id, category, type) values(nextval('flash_cards_cat_seq'), 'Medicine', 'TYPE_GLOBAL');
INSERT INTO flash_cards_categories(id, category, type) values(nextval('flash_cards_cat_seq'), 'Sport', 'TYPE_GLOBAL');
INSERT INTO flash_cards_categories(id, category, type) values(nextval('flash_cards_cat_seq'), 'Geography', 'TYPE_GLOBAL');

INSERT INTO flash_cards(id, english_Word, polish_word, flash_cards_category_id) values (nextval('flash_cards_seq'), 'Caterpillar', 'Gasienica', 1);
INSERT INTO flash_cards(id, english_Word, polish_word, flash_cards_category_id) values (nextval('flash_cards_seq'), 'Amphibians', 'Plazy', 1);
INSERT INTO flash_cards(id, english_Word, polish_word, flash_cards_category_id) values (nextval('flash_cards_seq'), 'Cat', 'Kot', 1 );
INSERT INTO flash_cards(id, english_Word, polish_word, flash_cards_category_id) values (nextval('flash_cards_seq'), 'Dog', 'Pies', 2 );
INSERT INTO flash_cards(id, english_Word, polish_word, flash_cards_category_id) values (nextval('flash_cards_seq'), 'Lion', 'Lew', 2 );

commit;