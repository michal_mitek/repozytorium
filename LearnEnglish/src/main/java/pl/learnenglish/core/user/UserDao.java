package pl.learnenglish.core.user;

import pl.learnenglish.core.BaseDao;

public interface UserDao extends BaseDao<User, Long> {
	User getByLogin(String login);
}
