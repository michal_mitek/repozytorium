package pl.learnenglish.core.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Scope(value = "session",  proxyMode = ScopedProxyMode.INTERFACES)
public class SessionUserProvider implements UserProvider {

	@Autowired
	private UserDao userDao;
	
	private User user;
	
	@Override
	public User getLoggedUser() {
		return userDao.getById(user.getId());
	}

	@Override
	@Transactional
	public void saveLoggedUser(String username) {
		user = userDao.getByLogin(username);				
	}

}
