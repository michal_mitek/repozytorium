package pl.learnenglish.core.user;

public enum UserRole {
	ROLE_USER,
	ROLE_ADMIN;
}
