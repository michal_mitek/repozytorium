package pl.learnenglish.core.user;


public interface UserProvider {

	User getLoggedUser();
	
	void saveLoggedUser(String username);
	
}
