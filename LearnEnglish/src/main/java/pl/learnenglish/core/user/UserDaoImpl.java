 package pl.learnenglish.core.user;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.learnenglish.core.AbstractBaseDao;

@Repository
public class UserDaoImpl extends AbstractBaseDao<User, Long> implements UserDao {

	@Override
	protected Class<User> supports() {
		return User.class;
	}
	
	@Override
	public User getByLogin(String login) {
		return (User) currentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("login", login))
				.uniqueResult();
	}
}
