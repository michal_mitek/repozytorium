package pl.learnenglish.core.flashCards;

import java.util.List;

import pl.learnenglish.core.BaseDao;

public interface FlashCardsDao extends BaseDao<FlashCards, Long>{
	FlashCards getByLogin(String login);
	List<FlashCards> getWordsListByCategoryId(Long id);
}
