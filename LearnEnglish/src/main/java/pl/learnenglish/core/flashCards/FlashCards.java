package pl.learnenglish.core.flashCards;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
public class FlashCards {

	@Id
	@GeneratedValue(generator="flashCardsSeq")
	@SequenceGenerator(name="flashCardsSeq", sequenceName="flash_cards_seq")
	private Long id;
	
	@Column
	private String englishWord;

	@Column
	private String polishWord;
	
	@ManyToOne
	@JoinColumn(name ="flash_cards_category_id")
	private FlashCardsCategories fsc;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnglishWord() {
		return englishWord;
	}

	public void setEnglishWord(String englishWord) {
		this.englishWord = englishWord;
	}

	public String getPolishWord() {
		return polishWord;
	}

	public void setPolishWord(String polishWord) {
		this.polishWord = polishWord;
	}

	public FlashCardsCategories getFsc() {
		return fsc;
	}

	public void setFsc(FlashCardsCategories fsc) {
		this.fsc = fsc;
	}
}
