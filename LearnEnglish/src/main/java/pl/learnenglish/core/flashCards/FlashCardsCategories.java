package pl.learnenglish.core.flashCards;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.context.annotation.PropertySource;


@Entity

public class FlashCardsCategories {

	@Id
	@GeneratedValue(generator="flashCardsCatSeq")
	@SequenceGenerator(name="flashCardsCatSeq", sequenceName="flash_cards_cat_seq")
	
	private Long id;
	
	@Column
	private String category;
	
	@Column
	@Enumerated(EnumType.STRING)
	private FlashCardCategoryType type;
	
	@OneToMany(mappedBy = "fsc", fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<FlashCards> fcList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<FlashCards> getFcList() {
		return fcList;
	}

	public void setFcList(List<FlashCards> fcList) {
		this.fcList = fcList;
	}

	public FlashCardCategoryType getType() {
		return type;
	}

	public void setType(FlashCardCategoryType type) {
		this.type = type;
	}
	
	

}
