package pl.learnenglish.core.flashCards;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.learnenglish.core.AbstractBaseDao;

@Repository
public class FlashCardsCategoriesDaoImpl extends AbstractBaseDao<FlashCardsCategories, Long> implements FlashCardsCategoriesDao{

	@Override
	protected Class<FlashCardsCategories> supports() {
		return FlashCardsCategories.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getFlashCardsCategoriesNames(){
		
		List<String> flashCardsCatNames = new ArrayList<String>();
		flashCardsCatNames = currentSession().createCriteria(FlashCardsCategories.class).add(Restrictions.sqlRestriction("1=1 ORDER BY RAND()")).list();
		return flashCardsCatNames;
		
	}
}
