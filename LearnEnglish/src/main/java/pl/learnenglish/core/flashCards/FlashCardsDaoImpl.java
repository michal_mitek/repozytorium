package pl.learnenglish.core.flashCards;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.learnenglish.core.AbstractBaseDao;


@Repository
public class FlashCardsDaoImpl extends AbstractBaseDao<FlashCards, Long> implements FlashCardsDao {

	
	
	@Override
	protected Class<FlashCards> supports() {
		return FlashCards.class;
	}
	
	@Override
	public FlashCards getById(Long id) {
		return (FlashCards) currentSession().createCriteria(FlashCards.class).add(Restrictions.eq("id", id)).uniqueResult();
	}

	@Override
	public FlashCards getByLogin(String login) {
		return (FlashCards) currentSession().createCriteria(FlashCards.class).add(Restrictions.eq("login", login)).uniqueResult();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FlashCards> getWordsListByCategoryId(Long id) {
		List<FlashCards> data = (List<FlashCards>) currentSession()
												   .createCriteria(FlashCards.class)
												   .add(Restrictions.eq("fsc.id", id))
												   .add(Restrictions.sqlRestriction("1=1 order by rand()"))
												   .list();
		return data;
	}
}
