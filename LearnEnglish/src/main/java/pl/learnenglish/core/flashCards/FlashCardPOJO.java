package pl.learnenglish.core.flashCards;

public class FlashCardPOJO {
	private Long id;
	private String pWord;
	private String eWord;
	
	public FlashCardPOJO() { }
	
	public FlashCardPOJO(Long id, String pWord, String eWord) {
		super();
		this.id = id;
		this.pWord = pWord;
		this.eWord = eWord;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getpWord() {
		return pWord;
	}
	public void setpWord(String pWord) {
		this.pWord = pWord;
	}
	public String geteWord() {
		return eWord;
	}
	public void seteWord(String eWord) {
		this.eWord = eWord;
	}
}
