package pl.learnenglish.core;

import java.io.Serializable;
import java.util.List;


public interface BaseDao <E, K extends Serializable>  {
	
	List<E> getAll();

	E getById(K key);

	void delete(E entity);

	void saveOrUpdate(E entity);


}
