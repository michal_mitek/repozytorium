package pl.learnenglish.web.register;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.learnenglish.core.user.User;
import pl.learnenglish.core.user.UserDao;
import pl.learnenglish.core.user.UserRole;

@Controller
public class RegisterController {

	@Autowired
	private UserDao userDao; 
	
	
	@RequestMapping(value="/login/register", method = RequestMethod.GET)
	public ModelAndView showRegisterView(){
		ModelAndView registerView = new ModelAndView("register");
		registerView.addObject("user", new User());
		return registerView;
	}

	@RequestMapping(value="/login/register", method = RequestMethod.POST)
	public ModelAndView addNewUser(@Valid @ModelAttribute User user, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("register");
			return modelAndView;
		}
		user.setRole(UserRole.ROLE_USER);
		userDao.saveOrUpdate(user);
		return new ModelAndView("login");
	}
}
