package pl.learnenglish.web.user.flashCards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.learnenglish.core.flashCards.FlashCardCategoryType;
import pl.learnenglish.core.flashCards.FlashCards;
import pl.learnenglish.core.flashCards.FlashCardsCategories;
import pl.learnenglish.core.flashCards.FlashCardsCategoriesDao;
import pl.learnenglish.core.flashCards.FlashCardsDao;

@Controller
public class FlashCardsController {
	
	@Autowired
	private FlashCardsCategoriesDao flashCardsCategoriesDao;
	
	@Autowired
	private FlashCardsDao flashCardsDao;
	
	@RequestMapping(value="/flashCards", method = RequestMethod.GET)
	public ModelAndView showFlashCardTemplate(@ModelAttribute FlashCardsCategories fcc, BindingResult bindingResult){
		ModelAndView modelAndView = new ModelAndView("flashCards");
		List<FlashCardsCategories> fcn = flashCardsCategoriesDao.getAll();
		modelAndView.addObject("categoriesNames", fcn);
		return modelAndView;
	}
	
	@RequestMapping(value="/flashCards/add/word", method = RequestMethod.POST)
	public String saveFlashCard(@RequestParam("userWordEnglish") String eWord,
									  @RequestParam("userWordPolish") String pWord,
									  @RequestParam("catId") Long catId){
		FlashCards fc = new FlashCards();
		fc.setEnglishWord(eWord);
		fc.setPolishWord(pWord);
		fc.setFsc(flashCardsCategoriesDao.getById(catId));
		flashCardsDao.saveOrUpdate(fc);
		return "redirect:/flashCards";
	}
	
	@RequestMapping(value="/flashCards/add/category", method = RequestMethod.POST)
	public String saveFlashCardCategory(@RequestParam("catName") String catName){
		FlashCardsCategories fcc = new FlashCardsCategories();
		fcc.setCategory(catName);
		fcc.setType(FlashCardCategoryType.TYPE_USER);
		flashCardsCategoriesDao.saveOrUpdate(fcc);
		return "redirect:/flashCards";
	}
	
	@RequestMapping(value="/flashCards/delete/category", method = RequestMethod.POST)
	public String deleteFlashCardCategory(@RequestParam ("catId") Long id){
		FlashCardsCategories fcc = flashCardsCategoriesDao.getById(id);
		flashCardsCategoriesDao.delete(fcc);
		return "redirect:/flashCards";
	}
	
	@RequestMapping("/showFlashCards/{id}")
	public ModelAndView showFlashCard(@PathVariable Long id){
		ModelAndView modelAndView = new ModelAndView("showFlashCards");
		modelAndView.addObject("currentCatId", id);
		modelAndView.addObject("flashCards", flashCardsDao.getById(id));
		modelAndView.addObject("fcList", flashCardsDao.getWordsListByCategoryId(id));
		return modelAndView;
	}
	
	
}
