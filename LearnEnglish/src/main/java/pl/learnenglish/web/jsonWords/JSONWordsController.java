package pl.learnenglish.web.jsonWords;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.learnenglish.core.flashCards.FlashCardPOJO;
import pl.learnenglish.core.flashCards.FlashCards;
import pl.learnenglish.core.flashCards.FlashCardsDao;

@Controller
public class JSONWordsController {
	
	@Autowired
	private FlashCardsDao flashCardsDao;

	@RequestMapping(value="/json/flashcards/{catId}", method = RequestMethod.GET)
	public @ResponseBody List<FlashCardPOJO> getJSONFlashCards(@PathVariable Long catId) {
		List<FlashCards> flashCardsByCategoryId = flashCardsDao.getWordsListByCategoryId(catId);
		List<FlashCardPOJO> flashCardPOJOList = new ArrayList<>();
		for(FlashCards flashCard : flashCardsByCategoryId) {
			flashCardPOJOList.add(new FlashCardPOJO(flashCard.getId(), flashCard.getPolishWord(), flashCard.getEnglishWord()));
		}
		return flashCardPOJOList;
	}
}