<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<html>
<head>
<spring:url value="/css/bootstrap.min.css" var="bootstrap" />
<spring:url value="/css/normalize.css" var="normalizeCss" />
<spring:url value="/css/style.css" var="styleCss" />
<spring:url value="/js/js.js" var="js" />

<link rel="stylesheet" href="${bootstrap}">
<link rel="stylesheet" href="${normalizeCss}">
<link rel="stylesheet" href="${styleCss}">

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src="${js}"></script>
</head>
<body>
	<c:url value="/login" var="logoutUrl" />
	<c:url value="/flashCards" var="flashCardsUrl" />

	<h2 class="welcomeTag">Hello : ${username} !</h2>
	
	<div align="center">
		<a href="${flashCardsUrl}" >Flash Cards</a>
	</div>
	
	<form action="${logoutUrl}" method="get" id="logoutForm">
		<input type="submit" value="Logout"></input>
	</form>

</body>
</html>