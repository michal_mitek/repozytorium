<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<spring:url value="/css/bootstrap.min.css" var="bootstrap" />
<spring:url value="/css/normalize.css" var="normalizeCss" />
<spring:url value="/css/style.css" var="styleCss" />
<spring:url value="/js/js.js" var="js" />
<spring:url value="/showFlashCards" var="sfc" />
<spring:url value="/flashCards/add/word" var="fc" />

<link rel="stylesheet" href="${bootstrap}">
<link rel="stylesheet" href="${normalizeCss}">
<link rel="stylesheet" href="${styleCss}">

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src="${js}"></script>
</head>
<body>

	<c:url value="/flashCardsShow" var="flashCardsShowUrl" />
	<h2>Flash Cards</h2>

	<%-- <form action="${flashCardsShowUrl}" method="post"
		id="flashCardsShowForm">
		<input type="submit" value="Flash cards"></input>
	</form>--%>

	<button id="showHideButton">Add new flash card</button>
	<div id="myForm">
		<form action="${fc}" method="POST">
			<input type="text" name="userWordEnglish" id="userWordEnglish">English
			word</input> <input type="text" name="userWordPolish" id="userWordPolish" />Polish
			word</input>
			<p>To category</p>
			<select name="catId">
				<c:forEach items="${categoriesNames}" var ="dropRecord">
					<option value="${dropRecord.id}"><c:out value="${dropRecord.category}"/></option>
				</c:forEach>
			</select>
			<button id="addFlashCardButton">Add new flash card</button>
		</form>
	</div>
	<table>
		<c:forEach items="${categoriesNames}" var="record">
			<tr>
				<td><a href="${sfc}/<c:out value="${record.id}" />">
				<c:out value="${record.category}" /></a></td>
			</tr>
		</c:forEach>
	</table>
	<spring:url value="/" var="homeAction" />
	<h2>
		<a href="${homeAction}">Home</a>
	</h2>
	
	<p>Add category</p>
	<spring:url value="/flashCards/add/category" var="fccAdd" />
	<form action="${fccAdd}" method="POST">
		<label>Category name:</label>
		<input type="text" name="catName" />
		<input type="submit" value="Add category" />
	</form>
	
	<p>Delete category<p>
	<spring:url value="/flashCards/delete/category" var="fccDelete" />
	<form action="${fccDelete}" method="POST">
		<label>Category name:</label>
			<select name="catId">
				<c:forEach items="${categoriesNames}" var ="dropRecord">
					<option value="${dropRecord.id}"><c:out value="${dropRecord.category}"/></option>
				</c:forEach>
			</select>
		<input type="submit" value="Delete category" />
	</form>

</body>
</html>