<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="utf-8" %>



<html>
<head>
	<meta charset="utf-8"/>
	<!--  <link href="css/login.css" rel="stylesheet" />-->
</head>
<body>

	<h1>Login</h1>

		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>

		<div class="test"></div>

		<div class="login-form">
			<form name='loginForm'action="<c:url value='/login' />" method='POST'>
				<table>
					<tr>
						<td>User:</td>
						<td><input type='text' name='username'></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type='password' name='password' /></td>
					</tr>
					<tr>
						<td colspan='2'><input name="submit" type="submit" value="submit" /></td>
						
					</tr>
				</table>
			</form>
			<form name='registerForm' action="<c:url value='/login/register' />" >
			<td colspan='2'><input name="register" type="submit" value="register" /></td>
			</form>
		</div>

</body>
</html>