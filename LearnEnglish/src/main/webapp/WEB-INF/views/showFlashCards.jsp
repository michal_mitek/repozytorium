<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<spring:url value="/css/bootstrap.min.css" var="bootstrap" />
<spring:url value="/css/normalize.css" var="normalizeCss" />
<spring:url value="/css/style.css" var="styleCss" />

<link rel="stylesheet" href="${bootstrap}">
<link rel="stylesheet" href="${normalizeCss}">
<link rel="stylesheet" href="${styleCss}">
<title>Insert title here</title>
</head>
<body>
	<h2>Witam</h2>
	
	<div>
	<span class="eWord"></span>
	<span class="pWord"></span>
	</div>

	<div class="btn-group" role="group" aria-label="...">
		<button type="button" class="btn btn-default" id="btnPrev">Previous</button>
		<button type="button" class="btn btn-default" id="btnCheck">Check</button>
		<button type="button" class="btn btn-default" id="btnNext">Next</button>
	</div>

	<spring:url value="/" var="homeAction" />
	<h2>
		<a href="${homeAction}">Home</a>
	</h2>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script>
$(document).ready(function() {
	$.ajax({
		url: "/learnEnglish/json/flashcards/${currentCatId}",
		dataType: "json",
		type: "GET",
		data: { rand: Math.random() * 1000000000 },
		success: function(data) {
			$('.eWord').html(data[0].eWord);
			$('.pWord').html(data[0].pWord);
			var currentFlashCardId = 0;
			$('#btnNext').click(function() {
				$('.pWord').hide();
				$('.eWord').show();
				if(currentFlashCardId < data.length - 1) {
					currentFlashCardId++;
					setWords(data[currentFlashCardId].pWord, data[currentFlashCardId].eWord);
				}
			});
			$('#btnPrev').click(function() {
				$('.pWord').hide();
				$('.eWord').show();
				if(currentFlashCardId > 0) {
					currentFlashCardId--;
					setWords(data[currentFlashCardId].pWord, data[currentFlashCardId].eWord);
				}
			});
		}
	});
	
	$('#btnCheck').click(function() {
		if($('.eWord').is(':visible')) {
			$('.eWord').hide();
			$('.pWord').show();
		} else {
			$('.pWord').hide();
			$('.eWord').show();
		}
	});
	
	function setWords(pWord, eWord) {
		$('.eWord').html(eWord);
		$('.pWord').html(pWord);
	}
});
	
</script>
</body>
</html>