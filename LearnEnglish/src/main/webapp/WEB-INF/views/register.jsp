<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8" />
</head>
<body>
	<h1>Register</h1>
	<spring:url value="/login/register" var="addNewUser" />

	<form:form method="post" modelAttribute="user" action="${addNewUser}">

		<p>
			<label>Type new user name:</label>
			<spring:bind path="login">
				<form:input path="login" id="login" />
				<form:errors path="login" />
			</spring:bind>
		</p>
		<p>
			<label>Type new user password:</label>
			<spring:bind path="password">
				<form:input path="password" id="password" />
				<form:errors path="password" />
			</spring:bind>
		</p>
		<p>
			<label>Type your full name:</label>
			<spring:bind path="fullName">
				<form:input path="fullName" id="fullName" />
				<form:errors path="fullName" />
			</spring:bind>
		</p>
		<p>
			<input name="submitUser" type="submit" value="Register new user" />
		</p>
	</form:form>

	<form name='goToLogin' action="<c:url value="/login" />">
		<td colspan='2'><input type="submit" value="Go to login page" /></td>
	</form>

</body>
</html>