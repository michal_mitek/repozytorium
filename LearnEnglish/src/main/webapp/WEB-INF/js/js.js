$(document).ready(function() {
	$('#showHideButton').click(function() {
		var myForm = $('#myForm');
		var button = $('#showHideButton');
		if (myForm.is(":visible")) {
			myForm.css('display', 'none');
			button.html('Add new flash card');
		} else {
			myForm.css('display', 'block');
			button.html('Cancel');
		}
	});
});
